package practical.operatorsandexpressions;

import java.util.Scanner;

public class Task7 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter angle");
		double a = sc.nextDouble();

		double sin = Math.sin(a);
		double cos = Math.cos(a);
		double tan = Math.tan(a);
		
		System.out.printf("angle(%f): Sin => %.4f Cos => %.4f Tan => %.4f\n", a, sin, cos, tan);
		sc.close();
	}
}
