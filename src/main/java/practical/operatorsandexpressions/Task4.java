package practical.operatorsandexpressions;

import java.util.Scanner;

public class Task4 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter a float");
		float a = sc.nextFloat();
		System.out.println("enter a float");
		float b = sc.nextFloat();
		float sum = a + b;
		float diff = a - b;
		float prod = a * b;
		float div = a / b;
		System.out.printf("With printf : Sum => %.4f Difference => %.4f Product => %.4f Division => %.4f\n", sum, diff, prod, div);
		System.out.printf("With println: Sum => " + dp4(sum) + " Difference => " + dp4(diff) + " Product => " + dp4(prod) + " Division => " + dp4(div));
		sc.close();
	}
	
	/**
	 * Note, this will not work for floats larger than Long.MAX_VALUE
	 * The real answer is to use printf
	 */
	static String dp4(float f) {
		long n = (long)(f * 10000);
		long q = n / 10000;
		long mod = n % 10000;
		
		String mods = String.valueOf(mod);
		if (mod < 1000) {
			mods = "0" + mods;
		}
		if (mod < 100) {
			mods = "0" + mods;
		}
		if (mod < 10) {
			mods = "0" + mods;
		}
		
		return "" + q + "." + mods;
	}
}
