package practical.operatorsandexpressions;

import java.util.Scanner;

public class Task5 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter radius");
		double r = sc.nextDouble();
		
		calculate(r, 3.14);
		calculate(r, Math.PI);
		sc.close();
	}
	
	private static void calculate(double r, double pi) {
		double circ = r * pi * 2;
		double area = pi * r * r;
		double surf = pi * r * r * 4;
		double vol = pi * r * r * r * 4 / 3;
		System.out.printf("Using pi(%f): Circ => %.4f Area => %.4f Surface => %.4f Volume => %.4f\n", pi, circ, area, surf, vol);
	}
}
