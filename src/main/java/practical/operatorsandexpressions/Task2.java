package practical.operatorsandexpressions;

import java.util.Scanner;

public class Task2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter an integer");
		int a = sc.nextInt();
		System.out.println("enter an integer");
		int b = sc.nextInt();
		long sum = a + b;
		long diff = a - b;
		long prod = a * b;
		System.out.println("Sum => " + sum + " Difference => " + diff + " Product => " + prod);
		sc.close();
	}
}
