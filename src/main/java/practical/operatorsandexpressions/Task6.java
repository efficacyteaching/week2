package practical.operatorsandexpressions;

import java.util.Scanner;

public class Task6 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter a");
		double a = sc.nextDouble();
		System.out.println("enter b");
		double b = sc.nextDouble();
		System.out.println("enter c");
		double c = sc.nextDouble();

		double root = Math.sqrt((b * b) - 4 * a * c);
		double x1 = (-b + root) / (a * 2);
		double x2 = (-b - root) / (a * 2);
		
		System.out.println("x => " + x1 + " or " + x2);
		sc.close();
	}
}
