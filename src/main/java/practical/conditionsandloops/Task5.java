package practical.conditionsandloops;

import java.util.Scanner;

public class Task5 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int value = 0;
		while (value <= 0) {
			System.out.println("enter a positive integer");
			value = sc.nextInt();
		}
		for (int i = 0; i < 21; ++i) {
			double result = Math.pow((double)value, (double)i);
			System.out.println("Step " + i + ": " + value + " to the power " + i + " is " + result);
		}
		sc.close();
	}
}
