package practical.conditionsandloops;

import java.util.Scanner;

public class Task4 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter numbers, one per line, negative to finish");
		int total = 0;
		boolean running = true;
		do {
			int value = sc.nextInt();
			if (value < 0) {
				running = false;
			} else {
				total += value;
			}
		} while(running && total <= 1024);
		System.out.println("Total => " + total);
		sc.close();
	}
}
