package practical.conditionsandloops;

import java.util.Scanner;

public class Task3 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter numbers, one per line, -1 to finish");
		int lowest = Integer.MAX_VALUE;
		int highest = Integer.MIN_VALUE;
		boolean running = true;
		do {
			int value = sc.nextInt();
			if (-1 == value) {
				running = false;
			} else {
				if (value < lowest) {
					lowest = value;
				}
				if (value > highest) {
					highest = value;
				}
			}
		} while(running);
		System.out.println("Lowest => " + lowest + ", Highest => " + highest);
		sc.close();
	}
}
