package practical.conditionsandloops;

public class Task1 {
	public static void main(String[] args) {
		if (args.length != 2) {
			System.err.println("usage: java Task1 number number");
			System.exit(1);
		}
		
		int n1 = Integer.parseInt(args[0]);
		int n2 = Integer.parseInt(args[1]);
		if (n2 > n1) {
			System.out.println(n1 + " " + n2);
		} else if (n1 > n2) {
			System.out.println(n2 + " " + n1);
		} else {
			System.out.println("The two numbers are the same");
		}
	}
}
