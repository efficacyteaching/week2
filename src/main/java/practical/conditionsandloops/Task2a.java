package practical.conditionsandloops;

import java.util.Scanner;

public class Task2a {
	public static void main(String[] args) {
		System.out.println("shape (T=triangle,S=square,R=rectanngle,C=circle)?");
		Scanner sc = new Scanner(System.in);
		char shape = sc.next().charAt(0);
		double area = 0;
		if (shape == 'T') {
			System.out.println("base?");
			double base = sc.nextDouble();
			System.out.println("height?");
			double height = sc.nextDouble();
			area = base * height / 2;
		} else if (shape == 'S') {
			System.out.println("side?");
			double side = sc.nextDouble();
			area = side * side;
		} else if (shape == 'R') {
			System.out.println("width?");
			double width = sc.nextDouble();
			System.out.println("height?");
			double height = sc.nextDouble();
			area = width * height;
		} else if (shape == 'C') {
			System.out.println("radius?");
			double radius = sc.nextDouble();
			area = Math.PI * radius * radius;
		} else {
			System.out.println("Unknown shape " + shape + " aborting.");
			System.exit(1);
		}
		System.out.println("area => " + area);
		sc.close();
	} 
}
