package practical.conditionsandloops;

public class Task6 {
	public static void main(String[] args) {
		for (int row = 1; row <= 12; ++row) {
			for (int col = 1; col <= 12; ++col ) {
				System.out.printf("%3d ", row * col);
			}
			System.out.println();
		}
	}
}
