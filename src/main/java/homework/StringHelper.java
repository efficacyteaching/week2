package homework;

public class StringHelper {
	public String fullName(String forename, String middleName, String surName) {
		
		// allow null for any field to mean "no name" without crashing
		if (null == forename) forename = "";
		if (null == middleName) middleName = "";
		if (null == surName) surName = "";
		
		// avoid multiple blanks in gaps or leading/trailing blanks
		forename = forename.trim();
		middleName = middleName.trim();
		surName = surName.trim();
		
		StringBuilder ret = new StringBuilder();
		
		if (!"".equals(forename)) {
			ret.append(forename);
		}
		
		if (!"".equals(middleName)) {
			if (ret.length() > 0) {
				ret.append(" ");
			}
			ret.append(middleName);
		}
		
		if (!"".equals(surName)) {
			if (ret.length() > 0) {
				ret.append(" ");
			}
			ret.append(surName);
		}
		
		return ret.toString();
	}
}
