package homework;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

/** tests for a string helper to format names for envelopes
 * 
 * Note that all the names used are birth names or professional names of real people.
 * I hope this give some idea of the complexity of even an apparently simple task
 *
 */
public class StringHelperTest {
	
	private StringHelper helper;

	public StringHelperTest() {
		this.helper = new StringHelper();
	}
	
	@Test
	public void testEmpty() {
		assertEquals("", helper.fullName("", "", ""));
		assertEquals("", helper.fullName(null, null, null));
	}
	
	@Test
	public void testOneName() {
		assertEquals("Prince", helper.fullName("Prince", "", ""));
		assertEquals("Madonna", helper.fullName("", "Madonna", ""));
		assertEquals("Pink", helper.fullName("", "", "Pink"));
	}
	
	@Test
	public void testNoMiddleName() {
		assertEquals("Patricia Carver", helper.fullName("Patricia", "", "Carver"));
		assertEquals("Joanne Rowling", helper.fullName("Joanne", "Rowling", ""));
		assertEquals("Abraham Lincoln", helper.fullName("", "Abraham", "Lincoln"));
	}
	
	@Test
	public void testThreeNames() {
		assertEquals("John Wilkes Booth", helper.fullName("John", "Wilkes", "Booth"));
		assertEquals("Robert Downey jr", helper.fullName("Robert", "", "Downey jr"));
		assertEquals("Jean Paul Gautier", helper.fullName("Jean Paul", "", "Gautier"));
	}
	
	@Test
	public void testMoreThanThreeNames() {
		assertEquals("Pablo Diego José Francisco de Paula Juan Nepomuceno María de los Remedios Cipriano de la Santísima Trinidad Ruiz y Picasso", helper.fullName("Pablo",  "Diego José Francisco de Paula Juan Nepomuceno María de los Remedios Cipriano de la Santísima Trinidad Ruiz y", "Picasso"));
		assertEquals("Dr Nicholas Hugh Mullen Caldwell", helper.fullName("Dr Nicholas", "Hugh Mullen", "Caldwell"));
	}
	
	@Test
	public void testNonRomanAlphabetCharacters() {
		assertEquals("Björk", helper.fullName("Björk", "", ""));
		assertEquals("Matt De'Ath", helper.fullName("Matt", "", "De'Ath"));
		assertEquals("Terry-Thomas", helper.fullName("Terry-Thomas", "", ""));
		assertEquals("李 敏", helper.fullName("李", "", "敏"));
	}
	
	@Test
	public void testWhitespaceCleaning() {
		assertEquals("Lee Harvey Oswald", helper.fullName("  Lee\t", "\n Harvey    ", " Oswald   "));
	}
	
	@Test
	public void testPreserveCase() {
		assertEquals("Christopher John van Es", helper.fullName("Christopher", "John", "van Es"));
	}

}
